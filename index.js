#!/usr/bin/env node

const child_process = require("node:child_process");
const fs = require("node:fs");
const util = require("node:util");

const settings = (() => {
  if (process.argv.length > 2) {
    try {
      return require(`./settings.${process.argv[2]}.js`);
    } catch (err) {
      console.error(`${err}`);
      process.exit(1);
    }
  }
  return require("./settings.js");
})();

const irc = require("irc");
const bless = require("./bless");

const ui = new bless.UserInterface();
console.log = function () {
  ui.log.pushLine(util.format.apply(util, arguments));
};

console.error = function () {
  ui.log.pushLine(util.format.apply(util, arguments));
};

function evalcmd(trigger, command, target, nick) {
  const prefix = nick != null ? `${nick}: ` : "";

  // sanitize input
  command = command.replace(/\1/g, "");

  const proc = child_process.spawn("./evalcmd", [trigger, command]);
  let prev = "";
  let lines = [];
  proc.stdout.on("data", (chunk) => {
    lines = (prev + chunk).split("\n");
    prev = lines.pop();
    for (const line of lines) client.say(target, `${prefix}${line}`);
  });
  proc.stdout.on("end", () => {
    if (prev.length > 0) client.say(target, `${prefix}${prev}`);
  });
  proc.on("close", (code) => {
    console.log(`${trigger}# ${command} # exit code ${code}`);
  });
}

function timestamp() {
  const d = new Date();
  return [
    ("0" + d.getHours()).slice(-2),
    ("0" + d.getMinutes()).slice(-2),
    ("0" + d.getSeconds()).slice(-2),
  ].join(":");
}

const client = new irc.Client(settings.server, settings.nick, settings.irc);

client.addListener("error", (err) => {
  console.log("error: ", err);
});
client.addListener("message", (from, to, message) => {
  console.log(
    `${timestamp()} ${to.padStart(10)} <${from.padStart(10)}> ${message}`,
  );
});
client.addListener("invite", (channel, from, message) => {
  console.log(`${timestamp()} invite to ${channel} from ${from}: ${JSON.stringify(message)}`);
  client.join(channel);
});
client.addListener("kick", (channel, who, by, reason) => {
  // If we are kicked from a channel, remove it from the irc library's
  // internal list of channels so we don't auto rejoin it after a netsplit
  if (who === client.nick) {
    console.log(`${timestamp()} kicked from ${channel} by ${by}. Reason ${reason}`);
    const i = client.opt.channels.indexOf(channel);
    if (i >= 0)
      client.opt.channels.splice(i, 1);
  }
});
client.addListener("kill", (nick, reason, channels, message) => {
  console.log("kill", nick, reason, channels, message);
});
client.addListener("registered", (message) => {
  if (client.nick !== settings.nick)
    client.say("nickserv", `GHOST ${settings.nick}`);
  console.log("registered", message);
});

client.addListener("notice", (nick, to, text) => {
  if (nick === "NickServ" && text.endsWith("has been ghosted"))
    client.send("NICK", settings.nick);
});

client.addListener("raw", (message) => {
  switch (message.command) {
    case "900": // You are now logged in as <userName>
      console.log(message.args[3]);
      break;
  }
});

client.addListener("ping", (server) => {
  console.log(server, "ping");
});

client.addListener("message#", (nick, channel, text) => {
  if (nick === client.nick) return;

  if (text === "# botsnack") return client.say(channel, "Core dumped");
  if (text === "# botsmack") return client.say(channel, "Segmentation fault");
  const m = text.match(/^([^ #]*)# (.*)/);
  if (m != null) evalcmd(m[1], m[2], channel, nick);
});

client.addListener("pm", (nick, text) => {
  if (nick === client.nick) return;

  if (text === "# botsnack") return client.say(nick, "Core dumped");
  if (text === "# botsmack") return client.say(nick, "Segmentation fault");
  const m = text.match(/^([^ #]*)# (.*)/);
  if (m != null) evalcmd(m[1], m[2], nick);
  else evalcmd("", text, nick);
});

ui.on("line", (value) => {
  let match;
  if ((match = value.match(/^say\s+([^\s]+)\s(.*)/)))
    client.say(match[1], match[2]);
  else if ((match = value.match(/^nick\s+(.+)/))) client.send("NICK", match[1]);
  else if ((match = value.match(/^(join|part)\s+(.+)/))) {
    if (match[1] === "join") client.join(match[2]);
    else client.part(match[2]);
  } else if ((match = value.match(/^(re|dis)?connect$/))) {
    if (match[0] === "disconnect") client.disconnect();
    else client.connect(0);
  } else console.log("Unknown command");
});
