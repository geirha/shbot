const { EventEmitter } = require("node:events");
const blessed = require("blessed");

class UserInterface extends EventEmitter {
  constructor() {
    super();
    this.screen = blessed.screen({ smartCSR: true });

    this.log = blessed.Log({
      top: 0,
      left: "center",
      width: "100%",
      height: "100%-3",
      border: { type: "line" },
    });

    this.input = blessed.Textbox({
      bottom: 0,
      left: "center",
      width: "100%",
      height: 3,
      border: { type: "line" },
    });

    this.input.on("focus", () => {
      this.input.readInput(onInput);
    });
    this.input.key(["escape", "C-c"], () => {
      process.exit(0);
    });

    const onInput = (err, value) => {
      if (err != null) this.log.pushLine(`-> ${err}`);
      this.log.pushLine(`-> ${value}`);
      this.emit("line", value);
      this.input.clearValue();
      this.screen.render();
      this.input.readInput(onInput);
    };

    this.screen.append(this.log);
    this.screen.append(this.input);

    this.input.focus();
    this.screen.render();
  }
}

module.exports = {
  UserInterface,
  userInterface: () => new UserInterface(),
};
